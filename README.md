# Sistemas Operativos 

## Ejercicios con Hilos (Threads) en C 

En este proyecto podréis encontrar ejercicios propuestos para practicar la programación con hilos.
Los ficheros de código C proporcionados están incompletos de manera que el estudiante solo tiene
que incluir la parte relacionada con la programación con hilos propiamente dicha.

Para compilar cada uno de los ejercicios, use la siguiente instrucción:
  
   gcc -pthread <fichero.c> -o fichero 
Esta es la lista de ejercicios disponible:

 - Ejercicio 1:
    Escriba un programa que cree un hilo al que se le pasan tres parámetros. El hilo debe imprimir los
    parámetros recibidos y terminar.
 
 - Ejercicio 2:
    Escriba un programa que reciba un número como parámetro y cree tantos hilos como indique ese número
    Cada hilo escribirá un mensaje como este: "Thread número X de Y". El hilo principal esperará por cada
    uno de los hilos creados y terminará.
 