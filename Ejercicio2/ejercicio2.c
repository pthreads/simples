#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <pthread.h>

#define MAX_THREAD 10


void *hello(void *arg)
{
  
  // AQUI FALTA CODIGO
  return (NULL);
}

int main(int argc, char* argv[]) {
  int n,i;
  pthread_t *threads;
  pthread_attr_t pthread_custom_attr;
  parm *p;

  if (argc != 2)
    {
      printf ("Usage: %s n\n  where n is no. of threads\n",argv[0]);
      exit(1);
    }

  n=atoi(argv[1]);

  if ((n < 1) || (n > MAX_THREAD))
    {
      printf ("The no of thread should between 1 and %d.\n",MAX_THREAD);
      exit(1);
    }

  threads=(pthread_t *)malloc(n*sizeof(*threads)); // Array using Heap memory
  pthread_attr_init(&pthread_custom_attr);

  /* Start up threads */

  for (i=0; i<n; i++)
    {
      
      pthread_create(&threads[i], &pthread_custom_attr, hello, (void *)(<QUE PONER AQUI?>));
    }

  /* Synchronize the completion of each thread. */

  for (i=0; i<n; i++)
    {
      pthread_join(threads[i],NULL);
    }
  
  return 0;
}
